# Debian 10 devops

A simple extension for the debian standard package, that include sudo for usage in devops, and we also 
upgrade to make sure we are in sync with security updates, and the we adds make and cmake, as they will 
be needed as a minimum.

This will use CI/CD to create a repos entry, for global consumption

## install in devops pipeline

```yaml
pool:
  vmImage: ubuntu-22.04

resources:
  containers:
    - container: sudo_debian
      image: "registry.gitlab.com/druppy/debian_10_devops:latest"

jobs:
    - job: test
      container: sudo_debian
      ...
```

## using it

In order to use this registry add this image `registry.gitlab.com/druppy/debian_10_devops:latest`