FROM debian:buster-slim

ENV DEBIAN_FRONTEND noninteractive
ENV FORCE_UNSAFE_CONFIGURE 1
ENV LANG C.UTF-8
ENV IDF_VERSION v4.4.4
ENV USCXML_VERSION 121887fe5775b1b7a2da39aca416e52caa47cc59
ENV PATH=/usr/local/bin:$PATH

# Configure locales
RUN apt-get update \
        && apt-get install -y sed locales \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*

RUN sed -i 's/# en_US\.UTF-8/en_US\.UTF-8/g' /etc/locale.gen
RUN locale-gen

# Install base dev-tools
RUN apt-get update && apt-get -y dist-upgrade \
        && apt-get install -y git build-essential cmake \
                       python3 python3-pip ruby gcovr \
                       curl wget default-jre-headless graphviz \
                       sudo patch libevent-dev \
                       bash-completion git-gui tig \
                       aria2 unzip \
                       ssh libffi-dev \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get -y install python3-bcrypt python3-yaml
#RUN pip3 install -U "bcrypt<4.0.0"
#RUN pip3 install pyyaml
RUN pip3 install --pre azure-cli 

COPY patch/idf/*.patch /patch/idf/
COPY patch/uscxml/*.patch /patch/uscxml/

# IDF install 
RUN wget -q https://dl.espressif.com/github_assets/espressif/esp-idf/releases/download/${IDF_VERSION}/esp-idf-${IDF_VERSION}.zip && \
        unzip -q esp-idf-${IDF_VERSION}.zip -d /usr/local && rm esp-idf-${IDF_VERSION}.zip && \
        patch -g0 -p1 -E -d /usr/local/esp-idf-${IDF_VERSION} -i /patch/idf/01-fix-i2c-sda-timing.patch -s -t -N 
#        /usr/local/esp-idf-${IDF_VERSION}/install.sh esp32s3

# build and install uSCXML 
RUN wget -q https://github.com/tklab-tud/uscxml/archive/${USCXML_VERSION}.zip && \
        unzip -q ${USCXML_VERSION}.zip -d src

RUN for patch in $(ls /patch/uscxml/*.patch); do \
        echo Applying uSCXML $patch; \
        if patch -g0 -p1 -E -d src/uscxml-${USCXML_VERSION} -i $patch -t -N; then \
           echo "Applied uSCXML patch $patch" ;\
        else \
           echo "Error $? in uSCXML patch $patch" ;\
           exit 1; \
        fi; \
    done;

RUN cmake -DCMAKE_CXX_FLAGS="-include functional" -S src/uscxml-${USCXML_VERSION} -B build && \
        cmake --build build && cmake --build build --target install && mv /build/deps/xerces-c/lib/* /usr/local/lib/ && \
        rm -r ${USCXML_VERSION}.zip src/uscxml-${USCXML_VERSION}
